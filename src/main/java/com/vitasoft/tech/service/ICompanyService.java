package com.vitasoft.tech.service;

import java.util.List;

import com.vitasoft.tech.entity.Company;



public interface ICompanyService {

	public Company getOneCompany(Integer id);
	public List<Company> getAllCompanies();
	public Integer createCompany(Company cob); 
}
