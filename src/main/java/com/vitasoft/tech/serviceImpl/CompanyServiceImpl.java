package com.vitasoft.tech.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.entity.Company;
import com.vitasoft.tech.exception.CompanyNotFoundException;
import com.vitasoft.tech.repository.CompanyRepository;
import com.vitasoft.tech.service.ICompanyService;

@Service
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	private CompanyRepository repo;
	
	public Company getOneCompany(Integer id) {
		return 
		repo.findById(id).orElseThrow(
				()->new CompanyNotFoundException("No Company Exist with Given id")
				);
		
	}

	public List<Company> getAllCompanies() {
		return repo.findAll();
	}

	public Integer createCompany(Company cob) {
		return repo.save(cob).getId();
	}

}
