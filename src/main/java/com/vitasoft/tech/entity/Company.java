package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company")
public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="company_id")
	private Integer id;
	
	@Column(name="cname")
	private String name;
	
	@Column(name="open")
	private Integer open_rate;
	
	@Column(name="close")
	private Integer close_rate;
	
	@Column(name="peak")
	private Integer peak_rate;
	
	@Column(name="least")
	private Integer least_rate;
	
	@Column(name="current")
	private Integer current_rate;
	
	@Column(name="marketcap")
	private String market_cap_code;
	
	@Column(name="volume")
	private Integer volume;

	@Column(name="address")
	private String address;
	
	@Column(name="stock_type")
	private String stock_type;
	
}
