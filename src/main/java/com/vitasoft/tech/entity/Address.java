package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="addr_tab")
public class Address {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="com_id_col")
	private Long id;
	
	@Column(name="addr_line1_col")
	private String line1;
	
	@Column(name="addr_line2_col")
	private String line2;
	
	@Column(name="addr_city_col")
	private String city;
	
	@Column(name="addr_state_col")
	private String State;
	
	@Column(name="addr_country_col")
	private String country;
	
	@Column(name="addr_pinCode_col")
	private Long pinCode;
	
//	@OneToOne(mappedBy="addr")
//	private Company cob;
	
	

}
