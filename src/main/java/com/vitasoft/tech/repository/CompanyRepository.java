package com.vitasoft.tech.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Company;

public interface CompanyRepository extends JpaRepository<Company,Integer> {

}
